package main

import (
	"fmt"
	"golang.org/x/tour/tree"
)

func doWalk(t *tree.Tree, ch chan int) {
	if t == nil {
		return
	}
	doWalk(t.Left, ch)
	ch <- t.Value
	doWalk(t.Right, ch)
}

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
	doWalk(t, ch)
	close(ch)
}

// Determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {

	ch1, ch2 := make(chan int), make(chan int)

	go Walk(t1, ch1)
	go Walk(t2, ch2)

	for {
		erg1, ok1 := <-ch1
		erg2, ok2 := <-ch2

		if !ok1 || !ok2 {
			return ok1 == ok2
		}

		if erg1 != erg2 {
			return false
		}
	}
}

func main() {
	fmt.Print("Same Tree Test: ")
	if Same(tree.New(1), tree.New(1)) {
		fmt.Println("OK")
	} else {
		fmt.Println("FAIL")
	}

	fmt.Print("Walk Test: ")
	ch := make(chan int)
	go Walk(tree.New(1), ch)
loop:
	for {
		erg, ok := <-ch
		if !ok {
			break loop
		}
		fmt.Printf(" %d", erg)
	}
}
