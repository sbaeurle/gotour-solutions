package main

import (
	"golang.org/x/tour/pic"
)

func Pic(dx, dy int) [][]uint8 {
	x := make([][]uint8, dy)

	for i := range x {
		x[i] = make([]uint8, dx)
	}

	for y, row := range x {
		for z := range row {
			row[z] = uint8(y + z)
		}
	}
	return x
}

func main() {
	pic.Show(Pic)
}
