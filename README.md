This repository contains individual solutions to the exervices of the Go tour.

Some of them are based on the offical solutions from https://github.com/golang/tour.

Feel free to use this repository as inspiration for your individual journey trough the Go tour.

More elegant and neat solutions are highly welcome! :)
