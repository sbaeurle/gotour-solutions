package main

import "fmt"

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	fib1 := 0
	fib2 := 1
	return func() int {
		var tmp int
		if fib1 < fib2 {
			tmp = fib1
			fib1 = fib1 + fib2
		} else {
			tmp = fib2
			fib2 = fib1 + fib2
		}
		return tmp
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 15; i++ {
		fmt.Println(f())
	}
}
