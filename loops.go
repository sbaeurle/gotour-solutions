package main

import (
	"fmt"
)

func Sqrt(x float64, acc float64) float64 {
	z := float64(x / 2)
	for i := float64(x - z*z); i > acc || -i > acc; {
		z -= (z*z - x) / (2 * z)
		i = x - z*z
	}
	return z
}

func main() {
	fmt.Println(Sqrt(2, 0.0000001))
}
