package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func rotate(b byte) byte {
	var begin, end byte
	switch {
	case b >= 'a' && b <= 'z':
		begin, end = 'a', 'z'
	case b >= 'A' && b <= 'Z':
		begin, end = 'A', 'Z'
	default:
		return b
	}

	return (b-begin+13)%(end-begin+1) + begin
}

func (rot13 rot13Reader) Read(p []byte) (n int, err error) {
	n, err = rot13.r.Read(p)

	for i := 0; i < n; i++ {
		p[i] = rotate(p[i])
	}

	return
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
